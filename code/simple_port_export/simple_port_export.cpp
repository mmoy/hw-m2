/********************************************************************
 * Copyright (C) 2018 by UCBL / LIP                                 *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

#include <systemc>

using namespace std;
using namespace sc_core;

struct simple_interface : sc_interface {
	virtual void simple_function(int) = 0;
};

struct Sender : sc_module {
	sc_port<simple_interface> out;

	void compute() {
		cout << "Calling simple_function on my sc_port ..." << endl;
		out->simple_function(42);
		cout << "Calling simple_function on my sc_port ... Done." << endl;
	}

	SC_HAS_PROCESS(Sender);
	Sender(sc_module_name name) : sc_module(name) {
		SC_THREAD(compute);
	}
	
};

struct Receiver : sc_module, simple_interface {
	sc_export<simple_interface> in;

	void simple_function(int val) override {
		cout << val << endl;
	}

	Receiver(sc_module_name name) : sc_module(name) {
		in.bind(*this);
	}
	
};

int sc_main(int, char **) {
	Sender s("sender");
	Receiver r("receiver");

	s.out.bind(r.in);

	sc_start();
	return 0;
}
