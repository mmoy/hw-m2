/********************************************************************
 * Copyright (C) 2012 by UCBL/LIP                                   *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

// Compiler avec g++ inheritance.cpp -o inheritance
// Executer avec ./inheritance

#include <iostream>

using namespace std;

class Base {
public:
	void normalMethod() {
		cout << "Base.normalMethod() appelée" << endl;
	}

	virtual void virtualMethod() {
		cout << "Base.virtualMethod() appelée" << endl;
	}
};

class Derived : public Base {
public:
	void normalMethod() {
		cout << "Derived.normalMethod() appelée" << endl;
	}
	
	virtual void virtualMethod() {
		cout << "Derived.virtualMethod() appelée" << endl;
	}
};

int main() {
	cout << "Instanciation avec pointeurs:" << endl;
	/*
	 * Pointer assignment. The object instanciated is used as-is
	 * and pB points to it. The static type of B is Base but its
	 * dynamic type is Derived.
	 */
	Derived *pD = new Derived();
	Base *pB = pD; /*
			* implicit upcast: pB can point to a derived
			* class's instance.
			*/
	cout << "Following a pointer to base class:" << endl;
	pB->normalMethod();
	pB->virtualMethod();

	cout << "Following a pointer to derived class:" << endl;
	pD->normalMethod();
	pD->virtualMethod();

	cout << "On-stack instanciation:" << endl;
	Derived d;
	/*
	 * Here we have a value assignment. The object is "too large"
	 * to fit in a variable of type Base, hence the assignment
	 * will make an implicit cas from Derived to Base. After this,
	 * b is both statically and dynamically of type Base, it
	 * doesn't have anything from a "Derived".
	 */
	Base b = d;
	b.normalMethod();
	b.virtualMethod();
}
