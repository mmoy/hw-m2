#include <systemc>

SC_MODULE(Counter)
{
	sc_core::sc_in<bool> clk;
	sc_core::sc_in<bool> reset;
	sc_core::sc_out<sc_dt::sc_uint<8> > count;

	SC_CTOR(Counter);

	void calcul();
};
