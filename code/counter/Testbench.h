#include <systemc>

SC_MODULE(Testbench)
{
	sc_core::sc_out<bool>         clk;
	sc_core::sc_out<bool>         reset;
	sc_core::sc_in<sc_dt::sc_uint<8> >   count;

	SC_CTOR(Testbench);

	void genClk();
	void genReset();
};
