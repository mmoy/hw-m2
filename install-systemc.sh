#!/bin/sh

mkdir -p /tmp/systemc
cd /tmp/systemc

sudo mkdir -p /opt/systemc-2.3.2
sudo chown `id -un` /opt/systemc-2.3.2
sudo chgrp `id -gn` /opt/systemc-2.3.2

wget http://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.2.tar.gz
tar xzvf systemc-2.3.2.tar.gz 
cd systemc-2.3.2
# C++11 standard needed or link might fail.
CXXFLAGS='-O3 -g -Wall -Wextra -Winvalid-pch -Wno-unused-parameter --std=gnu++11'
./configure --prefix=/opt/systemc-2.3.2
make -j 4
make install
