#!/bin/sh

mkdir -p /tmp/xilinx
cd /tmp/xilinx

# git.xilinx.com is down, and has been for a while :-(
# git clone git://git.xilinx.com/microblaze-gnu.git

wget https://matthieu-moy.fr/cours/hw-compil-simu/microblaze.tar.gz

sudo mkdir -p /opt/xilinx
sudo chown `id -un` /opt/xilinx
sudo chgrp `id -gn` /opt/xilinx

cd /opt/xilinx

tar xzvf /tmp/xilinx/microblaze.tar.gz

cd /opt/xilinx/microblaze/

echo "export PATH=$PWD/bin/:\"\$PATH\"" > setup.sh
echo "export CROSS_COMPILE=microblaze-unknown-linux-gnu-" >> setup.sh

echo "Installation completed."
echo "Please source $PWD/setup.sh to proceed."
