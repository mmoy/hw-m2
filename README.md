# Hardware Compilation and Simulation

Course given at [ENS-Lyon](http://www.ens-lyon.fr/DI/) by [Christophe
Alias](http://perso.ens-lyon.fr/christophe.alias/) and
[Matthieu Moy](https://matthieu-moy.fr/).

For more information, see [Hardware Compilation and
Simulation](http://perso.ens-lyon.fr/christophe.alias/teaching/master/).

# Installing SystemC

See install-systemc.sh

# Planning

## 1: Introduction to HW design flow (17/11)

* [01-introduction-slides.pdf](01-inroduction-slides.pdf)

<!-- Séance 1 : -> slide 45, « Transaction-Level Modeling ». -->
## 2: Introduction to Transaction-Level Modeling (24/11)

<!-- Séance 2 : 01-intro : end ? -->

## 3: C++ reminders, SystemC basics (1/12)

<!-- Séance 3 : 02-c-plus-plus, début de SystemC -> élaboration du premier expl -->

## 4: SystemC, TLM, start of lab (3/12)

<!--   TLM : 03-systemc modèle d'exéc (10 slides),
	   04-port-and-export : 
	   05-systemc-tlm :
	    - Sockets et routeurs (5 slides)
	    - Ensitlm (~20 slides) -->

## 5: Lab "First contact with SystemC/TLM", advanced TLM (uses of TLM) (8/12)


## 6: Advanced TLM (uses of TLM) (15/12)

## 7: End of lab, Parallelization of SystemC/TLM programs (5/1)

# Lab

See [TPs/lab-simu.pdf](TPs/lab-simu.pdf).

Deadline: January 10th 2021, 23:59. To be handed in at
https://tomuss-fr.univ-lyon1.fr/.
