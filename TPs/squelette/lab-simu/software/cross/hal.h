/********************************************************************
 * Copyright (C) 2009, 2012 by Verimag                              *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file hal.h
  \brief Harwdare Abstraction Layer : implementation for MicroBlaze
  ISS.


*/
#ifndef HAL_H
#define HAL_H

#include <stdint.h>

#define printf simple_printf
static inline void simple_printf(char *s);

#define abort simple_abort
static void simple_abort() {
	printf("abort() function called\r\n");
	_hw_exception_handler();
}

static inline int hal_read32(uint32_t a) {
	abort();
}

static inline void hal_write32(uint32_t a, uint32_t d) {
	abort();
}

static inline void hal_wait_for_irq() {
	abort();
}


static inline void simple_printf(char *s) {
	/* TODO: printf does nothing, for now ... */
}

void microblaze_enable_interrupts(void) {
	__asm("ori     r3, r0, 2\n"
	      "mts     rmsr, r3");
}

#endif /* HAL_H */
